import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tatianagolubeva on 5/8/16.
 */
public class MainTest {

    @Test
    public void serialize() throws Exception {
        Main main = new Main();
        assertTrue(main.serialize());
    }

    @Test
    public void unserialize() throws Exception {
        Main main = new Main();
        SerClass ser=new SerClass();
        assertEquals(ser.getClass(),main.unserialize().getClass());
    }

    @Test
    public void reflection() throws Exception {
        Main main = new Main();
        assertTrue(main.reflection(main.unserialize()));

    }

}
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

/**
 * Created by tatianagolubeva on 5/8/16.
 */
public class SerClassTest {

    @Test
    public void setNewRandNumb() throws Exception {
        SerClass serClass=new SerClass();
        Field numb = serClass.getClass().getDeclaredField("numb");
        numb.setAccessible(true);
        int numValue=(Integer) (numb.get(serClass));
        assertEquals(22,numValue);
        Method setNewRandNumb = serClass.getClass().getDeclaredMethod("setNewRandNumb");
        setNewRandNumb.setAccessible(true);
        setNewRandNumb.invoke(serClass);
        int numValue2 =(Integer) (numb.get(serClass));
        assertNotEquals(numValue2,22);


    }
}
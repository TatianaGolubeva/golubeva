import java.io.*;

/**
 * Created by tatianagolubeva on 4/11/16.
 */
public class Lab3_2 {
    public static void main(String[] args) throws IOException {
        // 5: Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма».
        //Вывести на экран полученную сумму.
        System.out.println("---- Задание 5 ----");
        int sum = 0;
        String value = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (!"сумма".equals(value)) {
            System.out.println("Введите число: ");
            value = br.readLine();
            try {
                Integer numb = Integer.valueOf(value);
                sum = sum + numb;
            } catch (NumberFormatException e) {
                if(!"сумма".equals(value)) {
                    System.out.println("Неверный формат числа");
                }
            }
        }

        System.out.println("Сумма равна: " + sum);
    }
}


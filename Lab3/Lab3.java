/**
 * Created by tan4ik on 11/04/2016.
 */
public class Lab3 {

    public static void main (String[] args){
        // 1: Дан массив из 4 чисел типа int. Сравнить их и вывести наименьшее на консоль
        System.out.println("---- Задание 1 ----");
        System.out.println("Исходный массив: ");
        int[] arr1={7,0,-15,15};
        for (int i:arr1)
            System.out.print(i+" ");
        int min=arr1[0];
        for (int i=0; i<arr1.length; i++) {
            if (min>arr1[i]) min=arr1[i];
        }
        System.out.println("\nМинимальный элемент: "+min);

        // 2: При помощи цикла for вывести на экран нечетные числа от 1 до 99.
        System.out.println();
        System.out.println("\n---- Задание 2 ----");
        System.out.println("Ряд нечетных чисел от 1 до 99: ");
        for (int i=1; i<=99;i+=2) {
            System.out.print(i+" ");
        }

        // 4: Дан массив из 4 чисел типа int. Вывести их все.
        System.out.println();
        System.out.println("\n---- Задание 4 ----");
        System.out.println("Вывод массива с помощью foreach: ");
        int[] arr2={5,0,4567,-34};
        for (int i:arr2)
            System.out.print(i+" ");
        System.out.println("\nВывод массива с помощью for: ");
        for (int j=0; j<arr2.length;j++)
            System.out.print(arr2[j]+" ");
        }

    }


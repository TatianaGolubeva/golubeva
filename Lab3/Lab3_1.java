import java.io.*;
import static java.lang.Math.*;

/**
 * Created by tatianagolubeva on 4/11/16.
 */
public class Lab3_1 {

    public static void main(String[] args) throws IOException {
        // 3: Вывести все простые числа от 1 до N. N задается пользователем через консоль
        System.out.println("---- Задание 3 ----");

        //Изначальный вариант
        /*String value = "";
        int numb = 0;
        int[] primeNumb = new int[20];
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (numb == 0) {
            System.out.println("Введите целое положительное число: ");
            try {
                value = br.readLine();
                numb = Integer.valueOf(value);
            } catch (NumberFormatException e) {
                System.out.println("Неправильный формат.\n");
            }

            if (numb > 0) {
                System.out.print("Простые числа до " + value + ": \n");
                for (int i = 2; i <= numb; i++) {
                    for (int j = 2; j <= i; j++) {
                        if (i % j == 0 && i / j != 1)
                            break;
                        else if (i % j == 0 && i / j == 1)
                            System.out.print(i + " ");
                    }
                }
            }
        }*/

        //Проапдейченый вариант
        System.out.println("Введите целое положительное число: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Integer value = Integer.valueOf(br.readLine());

        System.out.print("Простые числа до "+value+": \n2 ");
        for (int i = 3; i <= value; i+=2) {
            boolean isPrime = true;
            for (int j = 2; j <= sqrt(i); j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime){
                System.out.print(i+" ");
            }
        }



    }
}








/**
 * Created by tan4ik on 31/05/2016.
 */
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public class TestStackoverflow {
    private WebDriver driver;

    @Before
    public void SetUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://stackoverflow.com/");
    }

    @After
    public void afterMethod(){
        driver.close();
    }

    @Test //a. Проверить, что количество в табе ‘featured’ главного блока сайта больше 300
    public void checkFeaturedIsMoreThan300() throws Exception {
        WebElement bounty_indicator_tab = driver.findElement(By.xpath(".//*[@id='tabs']/a/span"));
        int count=Integer.valueOf(bounty_indicator_tab.getText());
        Assert.assertTrue("Features count is less than 300",count>300);
    }

    @Test //b.Кликнуть на главной странице Sing Up, проверить что на открывшейся странице присутствуют кнопки для входа из социальных сетей - Google, Facebook
    public void checkGoogleFacebookOnSignUpPage() throws Exception{
        WebElement lnk_topbar_signUp=driver.findElement(By.xpath(".//*[@class='topbar-menu-links']/a[contains(text(),'sign up')]"));
        lnk_topbar_signUp.click();
        Assert.assertTrue("There is no Google button on SignUp page", driver.findElement(By.xpath(".//span[text()='Google']")).isDisplayed());
        Assert.assertTrue("There is no Facebook button on SignUp page", driver.findElement(By.xpath(".//span[text()='Facebook']")).isDisplayed());
    }

    @Test //c. Кликнуть на главной странице любой вопрос из секции Top Questions и на открывшейся странице проверить, что вопрос был задан сегодня.
    public void checkQuestionAskedToday() throws Exception{
        WebElement lnk_question=driver.findElement(By.xpath(".//*[@id='question-mini-list']/div[5]/div[2]/h3/a"));
        lnk_question.click();
        WebElement txt_askedDate=driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr/td/p/b"));
        String askedDate=txt_askedDate.getText();
        boolean isTodaysDay=false;
        if (askedDate.equals("today"))
            isTodaysDay=true;
        Assert.assertTrue("Question was asked earlier than today",isTodaysDay);
    }

    @Test //d. проверить, что на главной странице stackoverflow есть предложение о работе с зарплатой больше $ 100k :) (тест должен упасть, если такого предложения нет).
    public void checkOffersWith100k() throws Exception{

        WebElement lnk_job_offer=driver.findElement(By.xpath(".//*[@id='hireme']"));
        String job_offer_title = lnk_job_offer.getText();

        Pattern p = Pattern.compile("([0-9]+)[K|k]");
        Matcher m = p.matcher(job_offer_title);

        List<String> allMatches = new ArrayList<String>();
        while (m.find()){
            allMatches.add(m.group(m.groupCount()));}
        //System.out.println(allMatches);
        List<Integer> allMatchesInt = new ArrayList<Integer>();
        for (Object str : allMatches) {
            allMatchesInt.add(Integer.parseInt((String)str));
        }
        boolean isFound=false;
        for (int x:allMatchesInt){
            if (x>100){
                isFound=true;
            }
            //System.out.println(x);
        }
        Assert.assertTrue("there is no offer with more than $100k salary",isFound);
    }

}
/**
 * Created by tatianagolubeva on 5/30/16.
 */

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestRozetka {
    private WebDriver driver;

    @Before
    public void SetUp() throws Exception{
        driver= new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://rozetka.com.ua");
    }

    @After
    public void afterMethod(){
        driver.close();
    }


    @Test // a. Проверить наличие лого розетка в левом верхнем углу главной страницы
    public void checkLogoIsDisplayed() throws Exception{
        WebElement ban_maintopleft_rozetka=driver.findElement(By.xpath(".//*[@class='logo']"));
        Assert.assertTrue("Can't find the banner",ban_maintopleft_rozetka.isDisplayed());
    }

    @Test //b. Проверить,что меню каталога товаров содержит пункт ‘Apple’
    public void checkMenuContainsApple() throws Exception{
        WebElement lnk_maincatalog_apple=driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'Apple')]"));
        Assert.assertTrue("Can't find link Apple in the menu",lnk_maincatalog_apple.isDisplayed());
    }

    @Test //c. Проверить, что меню каталога товаров сождержит секцию. Содержащую ‘MP3’
    public void checkMenuContainsMP3() throws Exception{
        WebElement lnk_mainctalog_mp3=driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'MP3')]"));
        Assert.assertTrue("Can't find the section in the menu with MP3", lnk_mainctalog_mp3.isDisplayed());
    }

    @Test //d. Кликнуть на секцию “Город” рядом с логтипом сайта, проверить что там присутствуют линки на города “Харьков”, “Одесса”, “Киев”
    public void checkCities() throws Exception{
        try {
            driver.findElement(By.xpath(".//*[@id='SubscribePushNotificationPanel']/div/div[@class='notificationPanelCross']")).click();
        } catch(Exception ex) {

        }
        WebElement lnk_maintop_city=driver.findElement(By.xpath(".//*[@class='xhr bold']"));
        lnk_maintop_city.click();
        Assert.assertTrue("Can't find  Киев among the cities", driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Киев')]")).isDisplayed());
        Assert.assertTrue("Can't find  Одесса among the cities", driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Одесса')]")).isDisplayed());
        Assert.assertTrue("Can't find  Харьков among the cities", driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Харьков')]")).isDisplayed());
    }

    @Test //e. Перейти в Корзину и проверить что она пуста
    public void checkCartIsEmpty() throws Exception{
        WebElement lnk_maintop_cart=driver.findElement(By.xpath(".//span[contains(text(),'Корзина')]"));
        lnk_maintop_cart.click();
        Assert.assertTrue("Cart is not empty", driver.findElement(By.xpath(".//*[@class='wrap-cart-empty']")).isDisplayed());
    }
}
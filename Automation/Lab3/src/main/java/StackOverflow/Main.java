package StackOverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by tatianagolubeva on 6/5/16.
 */
public class Main {
    public WebDriver driver;

    public Main(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath=".//*[@id='tabs']/a/span")
    public WebElement featuredTabCount;


    @FindBy(xpath=".//*[@class='topbar-menu-links']/a[contains(text(),'sign up')]")
    public WebElement signUpButton;

    public SignUp openSignUpPage(){
        signUpButton.click();
        return new SignUp(driver);
    }


    @FindBy(xpath=".//*[@id='question-mini-list']/div[5]/div[2]/h3/a")
    public WebElement questionLink;

    public QuestionSummary openQuestion(){
        questionLink.click();
        return new QuestionSummary(driver);
    }

    @FindBy(xpath=".//*[@id='hireme']")
    public WebElement jobDescription;


}

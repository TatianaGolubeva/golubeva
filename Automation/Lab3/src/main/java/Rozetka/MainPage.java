package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by tatianagolubeva on 6/5/16.
 */



public class MainPage {
    public WebDriver driver;

    public MainPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }


@FindBy (xpath=".//*[@class='logo']")
    public WebElement logo;


    @FindBy (xpath=".//*[@id='m-main']/li/a[contains(text(),'Apple')]")
    public WebElement appleItemFromMenu;


    @FindBy (xpath=".//*[@id='m-main']/li/a[contains(text(),'MP3')]")
    public WebElement itemWithMP3FromMenu;


    @FindBy(xpath=".//*[@class='xhr bold']")
    public WebElement cities;


    @FindBy (xpath=".//*[@class='header-city-i']/*[contains(text(),'Киев')]")
    public WebElement kievCity;

    @FindBy (xpath=".//*[@class='header-city-i']/*[contains(text(),'Одесса')]")
    public WebElement odessaCity;

    @FindBy (xpath=".//*[@class='header-city-i']/*[contains(text(),'Харьков')]")
    public WebElement kharkivCity;

    @FindBy (xpath=".//span[contains(text(),'Корзина')]")
    public WebElement cart;

    public Cart openCart(){
        cart.click();
        return new Cart (driver);
    }



}

import StackOverflow.Main;
import StackOverflow.QuestionSummary;
import StackOverflow.SignUp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tatianagolubeva on 6/5/16.
 */
public class TestStackoveflow {
    private static WebDriver driver;
    protected static Main main;
    protected static QuestionSummary questionSummary;
    protected static SignUp signUp;

    @Before
    public void SetUp(){
        driver=new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        main=new Main(driver);
    }

    @After
    public void after(){
        driver.close();
    }

    @Test
    public void checkFeaturedIsMoreThan300() throws Exception{
        int count=Integer.valueOf(main.featuredTabCount.getText());
        Assert.assertTrue("Features count is less than 300",count>300);
    }

    @Test
    public void checkGoogleFacebookOnSignUpPage() throws Exception{
        signUp=main.openSignUpPage();
        Assert.assertTrue("There is no Google button on SignUp page",signUp.googleButton.isDisplayed());
        Assert.assertTrue("There is no Facebook button on SignUp page",signUp.facebookButton.isDisplayed());
    }

    @Test
    public void checkQuestionAskedToday() throws Exception{
        questionSummary=main.openQuestion();
        String askedDate=questionSummary.askedText.getText();
        boolean isTodaysDay=false;
        if (askedDate.equals("today"))
            isTodaysDay=true;
        Assert.assertTrue("Question was asked earlier than today",isTodaysDay);
    }

    @Test
    public void checkOffersWith100k() throws Exception {
        String job_offer_title = main.jobDescription.getText();

        Pattern p = Pattern.compile("([0-9]+)[K|k]");
        Matcher m = p.matcher(job_offer_title);

        List<String> allMatches = new ArrayList<String>();
        while (m.find()) {
            allMatches.add(m.group(m.groupCount()));
        }
        List<Integer> allMatchesInt = new ArrayList<Integer>();
        for (Object str : allMatches) {
            allMatchesInt.add(Integer.parseInt((String) str));
        }
        boolean isFound = false;
        for (int x : allMatchesInt) {
            if (x > 100) {
                isFound = true;
            }
            System.out.println(x);
        }
        Assert.assertTrue("there is no offer with more than $100k salary", isFound);
    }
}

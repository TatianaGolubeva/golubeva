import Rozetka.Cart;
import Rozetka.MainPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by tatianagolubeva on 6/5/16.
 */
public class TestRozetka {
    private static WebDriver driver;
    protected static MainPage mainPage;
    protected static Cart cart;

    @Before
    public void SetUp(){
        driver=new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().window().maximize();
        mainPage=new MainPage(driver);
        try {
            driver.findElement(By.xpath(".//*[@id='SubscribePushNotificationPanel']/div/div[@class='notificationPanelCross']")).click();
        }
        catch(Exception ex) {}
    }

    @After
    public void after(){
        driver.close();
    }

    @Test
    public void checkLogoIsDisplayed()throws Exception{
        Assert.assertTrue("Can't find the banner",mainPage.logo.isDisplayed());
    }

    @Test
    public void checkMenuContainsApple() throws Exception{
        Assert.assertTrue("Can't find link Apple in the menu",mainPage.appleItemFromMenu.isDisplayed());
    }

    @Test
    public void checkMenuContainsMP3() throws Exception{
        Assert.assertTrue("Can't find the section in the menu with MP3",mainPage.itemWithMP3FromMenu.isDisplayed());
    }

    @Test
    public void checkCities() throws Exception{

        mainPage.cities.click();
        Assert.assertTrue("Can't find  Киев among the cities",mainPage.kievCity.isDisplayed());
        Assert.assertTrue("Can't find  Одесса among the cities",mainPage.odessaCity.isDisplayed());
        Assert.assertTrue("Can't find  Харьков among the cities",mainPage.kharkivCity.isDisplayed());
    }

    @Test
    public void checkCartIsEmpty() throws Exception{
        cart=mainPage.openCart();
        Assert.assertTrue("Cart is not empty",cart.emptyCart.isDisplayed());
    }
}

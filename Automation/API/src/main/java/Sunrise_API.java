import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tan4ik on 21/06/2016.
 */
public class Sunrise_API {

        List<URL> url = new ArrayList<URL>();
        List<String> expected = new ArrayList<String>();


        public List<URL> getURLandExpected ()throws IOException {
            File inputFile = new File("src/input_url_expected.txt");
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] splitedLine = line.split("\\s\\s+");
                url.add(new URL(splitedLine[0]));
                expected.add(splitedLine[1]);
            }
            return url;
        }

        public String getResponse(URL url) throws IOException {
            String response="";
            HttpURLConnection conn=(HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            BufferedReader resp=new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;
            while ((output=resp.readLine())!=null){
                response+=output;
                //System.out.println(output);
            }
            conn.disconnect();
            return response;
        }

    public boolean responseValidation(String response, String expected){
        boolean isContain=false;
        if (response.contains(expected)){
            isContain=true;}
        return isContain;
    }
}




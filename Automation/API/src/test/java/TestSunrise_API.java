import org.junit.*;

import java.io.IOException;

/**
 * Created by tatianagolubeva on 6/23/16.
 */
public class TestSunrise_API {

    @Test
    public void responseTest() throws IOException {
        Sunrise_API sunrise_api=new Sunrise_API();
        sunrise_api.getURLandExpected();
        for (int i=0; i<sunrise_api.url.size();i++){
            String response=sunrise_api.getResponse(sunrise_api.url.get(i));
            String expected=sunrise_api.expected.get(i);
            Assert.assertTrue("Expected is not found", sunrise_api.responseValidation(response,expected));
            System.out.println(i+1+") "+sunrise_api.expected.get(i)+" "+sunrise_api.responseValidation(response,expected));
        }
    }

}

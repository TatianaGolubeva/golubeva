@RozetkaAllTests

  Feature: check scenarios at the Rozetka

    @RozetkaTest1
    Scenario: check Rozetka logo is displayed
      Given I on rozetka main page
      Then I see Rozetka logo

    @RozetkaTest2
    Scenario: check main menu catalog contains Apple
      Given I on rozetka main page
      Then I see Apple item

    @RozetkaTest3
    Scenario: check main menu catalog contains item with MP3
      Given I on rozetka main page
      Then I see item with MP3

    @RozetkaTest4
    Scenario: check cities
      Given I on rozetka main page
      When I click Город
      Then I see cities

    @RozetkaTest5
    Scenario: check cart is empty
      Given I on rozetka main page
      When I click Корзина
      Then I see empty cart
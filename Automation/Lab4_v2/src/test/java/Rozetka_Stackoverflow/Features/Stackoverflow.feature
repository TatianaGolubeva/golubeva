@StackoverflowAllTests

  Feature: check scenarios at the Stackoverflow

    @StackoverflowTest1
    Scenario: check featured is more than 300
      Given I on stackoverflow main page
      Then count on featured tab is greater than 300

    @StackoverflowTest2
    Scenario: check Google and Facebook on Sign Up page
      Given I on stackoverflow main page
      When I click Sign Up
      Then I see Google and Facebook buttons

    @StackoverflowTest3
    Scenario: check question asked today
      Given I on stackoverflow main page
      When I click on any top question
      Then asked date is today

    @StackoverflowTest4
    Scenario: check offers with 100k
      Given I on stackoverflow main page
      Then I see offers with more than 100k salary
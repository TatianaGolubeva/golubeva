package Rozetka_Stackoverflow.Steps;

import Rozetka_Stackoverflow.Runner.Runner;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by tatianagolubeva on 6/7/16.
 */
public class StackoverflowSteps {
    @Given("^I on stackoverflow main page$")
    public void iOnStackoverflowMainPage() throws Throwable {
        Runner.driver.get("http://stackoverflow.com/");
        Runner.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Then("^count on featured tab is greater than (\\d+)$")
    public void countOnFeaturedTabIsGreaterThan(int arg0) throws Throwable {
        int count=Integer.valueOf(Runner.main.featuredTabCount.getText());
        Assert.assertTrue("Features count is less than 300",count>300);
    }

    @When("^I click Sign Up$")
    public void iClickSignUp() throws Throwable {
        Runner.signUp= Runner.main.openSignUpPage();
    }

    @Then("^I see Google and Facebook buttons$")
    public void iSeeGoogleAndFacebookButtons() throws Throwable {
        Assert.assertTrue("There is no Google button on SignUp page", Runner.signUp.googleButton.isDisplayed());
        Assert.assertTrue("There is no Facebook button on SignUp page", Runner.signUp.facebookButton.isDisplayed());

    }


    @When("^I click on any top question$")
    public void iClickOnAnyTopQuestion() throws Throwable {
       Runner.questionSummary= Runner.main.openQuestion();

    }

    @Then("^asked date is today$")
    public void askedDateIsToday() throws Throwable {
        String askedDate= Runner.questionSummary.askedText.getText();
        boolean isTodaysDay=false;
        if (askedDate.equals("today"))
            isTodaysDay=true;
        Assert.assertTrue("Question was asked earlier than today",isTodaysDay);

    }

    @Then("^I see offers with more than (\\d+)k salary$")
    public void iSeeOffersWithMoreThanKSalary(int arg0) throws Throwable {
        String job_offer_title = Runner.main.jobDescription.getText();

        Pattern p = Pattern.compile("([0-9]+)[K|k]");
        Matcher m = p.matcher(job_offer_title);

        List<String> allMatches = new ArrayList<String>();
        while (m.find()) {
            allMatches.add(m.group(m.groupCount()));
        }
        List<Integer> allMatchesInt = new ArrayList<Integer>();
        for (Object str : allMatches) {
            allMatchesInt.add(Integer.parseInt((String) str));
        }
        boolean isFound = false;
        for (int x : allMatchesInt) {
            if (x > 100) {
                isFound = true;
            }
            System.out.println(x);
        }
        Assert.assertTrue("there is no offer with more than $100k salary", isFound);
    }



}

package Rozetka_Stackoverflow.Runner;

/**
 * Created by tatianagolubeva on 6/6/16.
 */

import Rozetka.Cart;
import Rozetka.MainPage;
import StackOverflow.Main;
import StackOverflow.QuestionSummary;
import StackOverflow.SignUp;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
features="src/test/java/Rozetka_Stackoverflow/Features",
glue="Rozetka_Stackoverflow/Steps",
tags="@StackoverflowAllTests, @RozetkaAllTests")

public class Runner{
    public static WebDriver driver;
    public static MainPage mainPage;
    public static Cart cart;
    public static Main main;
    public static QuestionSummary questionSummary;
    public static SignUp signUp;


    @BeforeClass
    public static void SetUp(){
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        mainPage=new MainPage(driver);
        main=new Main(driver);
    }

    @AfterClass
    public static void afterClass(){
        driver.close();
    }

}

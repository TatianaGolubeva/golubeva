package Rozetka.Steps;

import Rozetka.Runner.Runner;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

/**
 * Created by tatianagolubeva on 6/6/16.
 */
public class RozetkaSteps {
    @Given("^I on rozetka main page$")
    public void iOnRozetkaMainPage() throws Throwable {
        Assert.assertTrue("Rozetka is not opened", Runner.driver.getCurrentUrl().contains("rozetka.com"));
    }

    @Then("^I see Rozetka logo$")
    public void iSeeRozetkaLogo() throws Throwable {
        Assert.assertTrue("Can't find the banner",Runner.mainPage.logo.isDisplayed());
    }

    @Then("^I see Apple item$")
    public void iSeeAppleItem() throws Throwable {
        Assert.assertTrue("Can't find link Apple in the menu",Runner.mainPage.appleItemFromMenu.isDisplayed());
    }

    @Then("^I see item with MP\\d+$")
    public void iSeeItemWithMP3() throws Throwable {
        Assert.assertTrue("Can't find the section in the menu with MP3",Runner.mainPage.itemWithMP3FromMenu.isDisplayed());
    }

    @When("^I click Город$")
    public void iClickГород() throws Throwable {
        Runner.mainPage.cities.click();
    }

    @Then("^I see cities$")
    public void iSeeCities() throws Throwable {
        Assert.assertTrue("Can't find  Киев among the cities",Runner.mainPage.kievCity.isDisplayed());
        Assert.assertTrue("Can't find  Одесса among the cities",Runner.mainPage.odessaCity.isDisplayed());
        Assert.assertTrue("Can't find  Харьков among the cities",Runner.mainPage.kharkivCity.isDisplayed());
    }

    @When("^I click Корзина$")
    public void iClickКорзина() throws Throwable {
     Runner.cart=Runner.mainPage.openCart();
    }

    @Then("^I see empty cart$")
    public void iSeeEmptyCart() throws Throwable {
        Assert.assertTrue("Cart is not empty",Runner.cart.emptyCart.isDisplayed());
    }
}

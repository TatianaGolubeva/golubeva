package Rozetka.Runner;

/**
 * Created by tatianagolubeva on 6/6/16.
 */

import Rozetka.Cart;
import Rozetka.MainPage;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
features="src/test/java/Rozetka/Features",
glue="Rozetka/Steps",
tags="@RozetkaAllTests")

public class Runner{
    public static WebDriver driver;
    public static MainPage mainPage;
    public static Cart cart;

    @BeforeClass
    public static void SetUp(){
        driver=new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://rozetka.com.ua/");
        mainPage=new MainPage(driver);
        try {
            driver.findElement(By.xpath(".//*[@id='SubscribePushNotificationPanel']/div/div[@class='notificationPanelCross']")).click();
        }
        catch(Exception ex) {}
    }

    @AfterClass
    public static void afterClass(){
        driver.close();
    }

}

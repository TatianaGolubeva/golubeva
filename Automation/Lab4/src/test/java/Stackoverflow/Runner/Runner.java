package Stackoverflow.Runner;

import StackOverflow.Main;
import StackOverflow.QuestionSummary;
import StackOverflow.SignUp;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by tatianagolubeva on 6/7/16.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/Stackoverflow/Features",
        glue = "Stackoverflow/Steps",
        tags = "@StackoverflowAllTests")


public class Runner {
        public static WebDriver driver;
        public static Main main;
        public static QuestionSummary questionSummary;
        public static SignUp signUp;

        @BeforeClass
        public static void SetUp() {
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
            driver.get("http://stackoverflow.com/");
            main = new Main(driver);
        }

        @AfterClass
        public static void afterClass() {
            driver.close();
        }
    }


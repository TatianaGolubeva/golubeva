package StackOverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by tatianagolubeva on 6/5/16.
 */
public class QuestionSummary {
    public WebDriver driver;

    public QuestionSummary(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath=".//*[@id='qinfo']/tbody/tr/td/p/b")
    public WebElement askedText;


    @FindBy(xpath=".//*[@id='hlogo']/a")
    public WebElement logo;

    public Main returnHome(){
        logo.click();
        return new Main(driver);
    }


}

/**
 * Created by tatianagolubeva on 4/8/16.
 */
import java.io.*;


public class Lab2_1 {
    public static void main(String[] args) {
        /* Написать программу которая бы запросила пользователя ввести число,
        затем знак математической операции, затем еще число и вывела результат.
        Должно работать и для целых и для дробных чисел
         */
        System.out.println("---- Задание 4 ----");
        float rez = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите первое число: ");
            float numb1 = Float.valueOf(br.readLine());
            System.out.println("Введите знак: ");
            String znak = String.valueOf(br.readLine());
            char znak1 = znak.charAt(0);
            System.out.println("Введите второе число: ");
            float numb2 = Float.valueOf(br.readLine());
            //Вариант 1: изпользуя if
            if (znak1 == '+') {
                rez = numb1 + numb2;
            } else if (znak1 == '-') {
                rez = numb1 - numb2;
            } else if (znak1 == '*') {
                rez = numb1 * numb2;
            } else if (znak1 == '/' && numb2==0) {
                    System.out.println("На ноль делить нельзя");
                    return;
                } else {
                    rez = numb1 / numb2;
                }
                System.out.println("Результат: "+ rez);

            // Вариант 2: используя switch
            /*switch (znak1) {
                case '+':
                    rez=numb1+numb2;
                    break;
                case '-':
                    rez=numb1-numb2;
                    break;
                case '*':
                    rez=numb1*numb2;
                    break;
                case '/':
                    if (numb2==0) {
                        System.out.println("На ноль делить нельзя!");
                        return;
                    }
                    break;
            }
            System.out.println("Результат: "+ rez);
*/
            }
           catch(IOException e){
               e.printStackTrace();
            }
        catch(NumberFormatException e){
          System.out.println("Неправильный формат");
        }


        }
    }



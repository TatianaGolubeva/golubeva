/**
 * Created by tan4ik on 08/04/2016.
 */
public class Lab2 {
    public static void main(String[] args) {

   /* Напишите примеры использования данных операторов и выведите результаты на экран
    a. Арифметические
    b. Сравнения
    с. Логические
    d. Приравнивания
    */
        int a=365;
        int b=13;
        int c=15;
        int mltplic=0;
        boolean cmpr=true;
        boolean lgc=true;
        int eql=0;
        System.out.println("---- Задание 1 ----");
        System.out.println("Пример использования операций: ");
        mltplic=a*b;
        System.out.println("Арифметическая: "+ a + "*" + b + "=" + mltplic);
        cmpr=a>b;
        System.out.println("Сравнение: "+ a + ">" + b + " - " + cmpr);
        lgc=a>b && b>c;
        System.out.println("Логическая: "+ a + ">" + b + " и " + b + ">" + c +" - " + lgc);
        eql=mltplic;
        System.out.println("Приcвоение: " + eql);


        //Переведите значение каждого примитивного типа в строку и обратно - выведите результаты на  экран
        System.out.println ();
        System.out.println("\n---- Задание 2 ----");
        int d=-9876;
        long e=65656565656565L;
        double f=8.8e10;
        float g=1576.9888f;
        char h='r';
        byte i=-128;
        short j=32767;
        boolean k=true;
        String str=Integer.toString(d);
        System.out.println("Результат int to String: "+ str);
        Integer str1=Integer.valueOf(str);
        System.out.println("Результат String to int: "+ str1);
        String lng=Long.toString(e);
        System.out.println("\nРезультат long to String: "+ lng);
        Long lng1=Long.valueOf(lng);
        System.out.println("Результат String to long: "+ lng1);
        String dbl=Double.toString(f);
        System.out.println("\nРезультат double to String: "+ dbl);
        Double dbl1=Double.valueOf(dbl);
        System.out.println("Результат String to double: "+ dbl1);
        String flt=Float.toString (g);
        System.out.println("\nРезультат float to String: "+ flt);
        Float flt1=Float.valueOf(flt);
        System.out.println("Результат String to float: "+ flt1);
        String chToStr=Character.toString(h);
        System.out.println("\nРезультат char to String: "+ chToStr);
        char chToStr1=chToStr.charAt(0);
        System.out.println("Результат String to char: "+ chToStr1);
        String bt=Byte.toString(i);
        System.out.println("\nРезультат byte to String: "+ bt);
        Byte bt1=Byte.valueOf(bt);
        System.out.println("Результат String to byte: "+ bt1);
        String sht=Short.toString(j);
        System.out.println("\nРезультат short to String: "+ sht);
        Short sht1=Short.valueOf(sht);
        System.out.println("Результат String to short: "+ sht1);
        String bln=Boolean.toString(k);
        System.out.println("\nРезультат boolean to String: "+ bln);
        Boolean bln1=Boolean.valueOf(bln);
        System.out.println("Результат String to boolean: "+ bln1);


        //Переведите число с точкой в тип без точки и наоборот - выведите результаты
        System.out.println ();
        System.out.println("\n---- Задание 3 ----");
        double l=345.765;
        long m=-987654l;
        int l1=(int) l;
        System.out.println("Результат double ("+l+") to int: "+ l1);
        double m1=(double) m;
        System.out.println("Результат long ("+m+") to double: "+ m1);


    }
}
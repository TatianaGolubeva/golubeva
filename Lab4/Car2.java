package Lab4;

import Lab4.Car;

/**
 * Created by tatianagolubeva on 4/14/16.
 */
public class Car2 extends Car {


    Car2(String name, int maxSpeed, int acceleration, float mobility) {
        super(name, maxSpeed, acceleration, mobility);
    }

    public void turn() {
        if (curSpeed<0.5*maxSpeed) {
            acceleration*=2;
        }
    }
}

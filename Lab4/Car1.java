package Lab4;

import Lab4.Car;

/**
 * Created by tatianagolubeva on 4/14/16.
 */
public class Car1 extends Car {


    Car1(String name, int maxSpeed, int acceleration, float mobility) {
        super(name, maxSpeed, acceleration, mobility);
    }

    public void turn() {
        if (curSpeed>0.5*maxSpeed) {
            mobility=(float)(mobility+0.005*(curSpeed-0.5*maxSpeed));
        }

    }
}

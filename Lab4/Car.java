package Lab4;

import static java.lang.Math.sqrt;

/**
 * Created by tatianagolubeva on 4/14/16.
 */
public abstract class Car {
    String name;
    int maxSpeed;
    int acceleration;
    float mobility;
    int time=0;
    int curSpeed=0;



    Car (String name, int maxSpeed, int acceleration, float mobility) {
        this.name=name;
        this.maxSpeed=maxSpeed;
        this.acceleration=acceleration;
        this.mobility=mobility;
    }


    abstract void turn();

    public  int drive () {
        time=time+(int)(sqrt(curSpeed*curSpeed+2*acceleration*2000)-curSpeed)/acceleration;
        curSpeed=curSpeed+acceleration*time;
        if (curSpeed>=maxSpeed) {
            curSpeed=maxSpeed;
        }
        turn();
        curSpeed*=(int)mobility;
        return time;

    }

    }




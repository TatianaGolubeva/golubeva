package Lab5;

import java.util.*;

/**
 * Created by tatianagolubeva on 4/20/16.
 */
public class MyList implements List {
    public Object[] myList;

    MyList(Object[] myList){
        this.myList=myList;
    }

    @Override
    public int size() {
    int listSize=myList.length;
      return listSize;
    //return myList.length;
    }

    @Override
    public boolean isEmpty() {
        if (myList.length==0) {
        return true;
        }
        else {
        return false;
        }
    }

    @Override
    public boolean contains(Object o) {
        boolean isContain=false;
        for (int i = 0; i < myList.length; i++) {
            if (myList[i].equals(o))
            {
                isContain=true;
                break;
               }
            else
                isContain=false;
        }
        if (isContain==true) {
            return true;
            }
        else return false;
    }



    @Override
    public Iterator iterator() {
        return new Itr();
    }

    class Itr implements Iterator {
        int nextElem=0;
        @Override
        public boolean hasNext() {
            if (nextElem<myList.length)
            return true;
            else return false;
        }
        @Override
        public Object next() {
            nextElem +=1;
            return myList[nextElem];
        }

        //@Override
        //public void remove() {
        //}
    }

    @Override
    public Object[] toArray() {
        return myList;
    }

    //List interface doesn't contain this method
    public void increaseListSize() {
        myList = Arrays.copyOf(myList, myList.length + 1);
    }

    @Override
    public boolean add(Object o) {
        increaseListSize();
        myList[myList.length-1]=o;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        boolean isRemoved=false;
        for (int i=0; i<myList.length; i++){
            if (myList[i].equals(o)){
            MyList.this.remove(i);
                isRemoved=true;
                break;
            } else isRemoved=false;
        }
        return isRemoved;

    }

    @Override
    public boolean addAll(Collection c) {
        Object[] collectionToAppend=c.toArray();
        Object[] concatenatedArrays;
        concatenatedArrays=Arrays.copyOf(myList, myList.length + collectionToAppend.length);
        System.arraycopy(collectionToAppend,0,concatenatedArrays,myList.length, collectionToAppend.length);
        myList=concatenatedArrays;
        return true;

    }

    @Override
    public boolean addAll(int index, Collection c) {
        Object[] collectionToAppend=c.toArray();
        Object[] concatenatedArrays=new Object[myList.length+collectionToAppend.length];
        System.arraycopy(myList,0,concatenatedArrays,0,index);
        System.arraycopy(collectionToAppend,0, concatenatedArrays,index, collectionToAppend.length);
        System.arraycopy(myList,index+1,concatenatedArrays, index+collectionToAppend.length, concatenatedArrays.length);
        myList=concatenatedArrays;
        return true;
    }

    @Override
    public void clear() {
        for (int i=0;i<size();i++) {
            myList[i]=null;
        }
    }

    @Override
    public Object get(int index) {
        return myList[index];
    }

    @Override
    public Object set(int index, Object element) {
        Object oldValue=myList[index];
        myList[index]=element;
        return oldValue;
    }

    @Override
    public void add(int index, Object element) {
        Object[] concatenatedArrays=new Object[myList.length+1];
        System.arraycopy(myList,0,concatenatedArrays,0,index);
        concatenatedArrays[index]=element;
        System.arraycopy(myList,index+1,concatenatedArrays, index+1, concatenatedArrays.length);
        myList=concatenatedArrays;
    }

    @Override
    public Object remove(int index) {
        if (index>=myList.length){
            throw new ArrayIndexOutOfBoundsException();
        }
            Object removedElem = myList[index-1];
            for(int i=index-1; i<MyList.this.size();i++ ){
                if (i==myList.length-1){
                    break;
                }
                else {
                    myList[i] = myList[i+1];
                }
             }
             //myList[myList.length-1] = null;
    return removedElem;

    }

    @Override
    public int indexOf(Object o) {
        int indexOf=-1;
        for (int i=0; i<myList.length; i++){
            if (o.equals(myList[i])){
                indexOf=i;
                break;
            } else indexOf=-1;
        }
        return indexOf;
    }

    @Override
    public int lastIndexOf(Object o) {
        int lastIndexOf=-1;
        for (int i=myList.length; i>=0; i--){
            if (o.equals(myList[i])){
                lastIndexOf=i;
                break;
            } else lastIndexOf=-1;
        }
        return lastIndexOf;
    }

    @Override
    public ListIterator listIterator() {
        return new ListItr(0);
    }


    @Override
    public ListIterator listIterator(int index) {
        return new ListItr(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return myList;
    }


    private class ListItr extends Itr implements ListIterator {
        ListItr(int index) {
            super();
            nextElem = index;
        }

       //@Override
       // public boolean hasNext() {
       // return false;
       // }

       // @Override
       // public Object next() {
       //     return null;
       // }

        @Override
        public boolean hasPrevious() {
            return nextElem != 0;
        }

        @Override
        public Object previous() {
            int i=nextElem-1;
            nextElem=i;
            return myList[nextElem];
        }

        @Override
        public int nextIndex() {
            return nextElem;
        }

        @Override
        public int previousIndex() {
            return nextElem-1;
        }

        @Override
        public void remove() {
            MyList.this.remove(nextElem);
        }

        @Override
        public void set(Object o) {
            MyList.this.set(nextElem, o);
        }

        @Override
        public void add(Object o) {
            MyList.this.add(nextElem, o);
        }
    }
}
